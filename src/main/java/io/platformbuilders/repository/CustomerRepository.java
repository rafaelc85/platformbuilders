package io.platformbuilders.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.platformbuilders.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	@Query(value = "SELECT * FROM customer WHERE LOWER(nome) LIKE %?1%", nativeQuery = true)
	public Page<Customer> findByNome(String nome, Pageable pageable);
	
	@Query(value = "SELECT * FROM customer WHERE cpf = ?1", nativeQuery = true)
	public Page<Customer> findByCpf(String cpf, Pageable pageable);
	
	@Query(value = "SELECT * FROM customer WHERE LOWER(nome) LIKE %?1% and cpf = ?2", nativeQuery = true)
	public Page<Customer> findByNameAndCpf(String nome, String cpf, Pageable pageable);
	
	public Page<Customer> findAll(Pageable pageable);
}
