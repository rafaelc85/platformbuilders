package io.platformbuilders.populator;

import javax.annotation.Resource;

import io.platformbuilders.model.Customer;

@Resource
public class CustomerPopulator {
	
	public static Customer populate(Customer dbCustomer, Customer customer) {
		if(customer.getCpf()!=null) dbCustomer.setCpf(customer.getCpf());
		if(customer.getNome()!=null) dbCustomer.setNome(customer.getNome());
		if(customer.getDataNascimento()!=null) 
			dbCustomer.setDataNascimento(customer.getDataNascimento());
		return dbCustomer;
	}
}
