package io.platformbuilders.exception;

public class InvalidCustomerException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public InvalidCustomerException(String errorMessage) {
		super(errorMessage);
	}
}
