package io.platformbuilders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.platformbuilders.model.Customer;
import io.platformbuilders.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	@PostMapping
	public ResponseEntity<Long> createCustomer(@RequestBody Customer customer) {
		customerService.saveCustomer(customer);
		return new ResponseEntity<>(customer.getId(), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<Page<Customer>> viewCustomers(@RequestParam(required = false) String nome, 
			@RequestParam(required = false) String cpf,
			@RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "100") int size) {
		return new ResponseEntity<>(
				customerService.getCustomers(nome, cpf, page, size), 
				HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Customer> viewCustomerById(@PathVariable ("id") Long id) {
		return new ResponseEntity<>(customerService.getCustomer(id),
				HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Customer> removeCustomerById(@PathVariable ("id") Long id) {
		return new ResponseEntity<>(customerService.removeCustomer(id),
				HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Customer> updateCustomerById(@PathVariable ("id") Long id, 
			@RequestBody Customer customer) {
		return new ResponseEntity<>(
				customerService.updateCustomer(id,customer),
				HttpStatus.OK);
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<Customer> updateCustomerNameByIdCond(@PathVariable ("id") Long id, 
			@RequestBody Customer customer) {
		return new ResponseEntity<>(
				customerService.updateCustomerName(id, customer.getNome()),
				HttpStatus.OK);
	}
}
