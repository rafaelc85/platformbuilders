package io.platformbuilders.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.platformbuilders.exception.InvalidCustomerException;
import io.platformbuilders.model.Customer;
import io.platformbuilders.populator.CustomerPopulator;
import io.platformbuilders.repository.CustomerRepository;
import io.platformbuilders.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	CustomerRepository customerRepository;
	
	public Customer getCustomer(Long id) {
		Optional<Customer> customer = customerRepository.findById(id);
		if(customer.isPresent()) {
			return prepareCustomer(customer.get());
		}
		throw new InvalidCustomerException("Invalid customer");
	}

	public void saveCustomer(Customer customer) {
		customer = formatCustomerBeforeSaving(customer);
		customerRepository.save(customer);
	}

	public Customer removeCustomer(Long id) {
		Customer customer = this.getCustomer(id);
		customerRepository.delete(customer);
		return customer;
	}

	public Customer updateCustomer(Long id, Customer customer) {
		Customer dbCustomer = this.getCustomer(id);
		dbCustomer = CustomerPopulator.populate(dbCustomer, customer);
		dbCustomer = formatCustomerBeforeSaving(dbCustomer);
		return prepareCustomer(customerRepository.save(dbCustomer));		
	}

	public Customer updateCustomerName(Long id, String nome) {
		Customer customer = this.getCustomer(id);
		customer.setNome(nome);
		return prepareCustomer(customerRepository.save(customer));
	}

	public Page<Customer> getCustomers(String nome, String cpf, int page, int size) {
		PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "id");
		
		if(cpf!=null) {
			cpf = cpf.replaceAll("\\D+","");
			if(nome!=null) {
				nome = nome.toLowerCase();
				return prepareCustomerList(customerRepository.findByNameAndCpf(nome, cpf, pageRequest) ,size, pageRequest);
			}else {
				return prepareCustomerList(customerRepository.findByCpf(cpf, pageRequest) ,size, pageRequest);
			}			
		}
		if(nome!=null) {
			nome = nome.toLowerCase();
			return prepareCustomerList(customerRepository.findByNome(nome, pageRequest) , size, pageRequest);
		}		
		return prepareCustomerList(customerRepository.findAll(pageRequest), size, pageRequest);
	}
	
	protected Page<Customer> prepareCustomerList(Iterable<Customer> customers, int size, PageRequest pageRequest){
		for (Customer customer : customers) {
			customer = prepareCustomer(customer);
		}
		List<Customer> customersList = IteratorUtils.toList(customers.iterator());
		
		Page<Customer> customersPage = new PageImpl<>(
				customersList, 
                pageRequest, size);
		return customersPage;
	}
	
	protected Customer prepareCustomer(Customer customer) {
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		if(customer.getDataNascimento() == null) return customer;
		int d1 = Integer.parseInt(formatter.format(customer.getDataNascimento()));                            
	    int d2 = Integer.parseInt(formatter.format(new Date()));  
	    int age = (d2 - d1) / 10000;
	    customer.setIdade(age);
	    return customer;
	}
	
	protected Customer formatCustomerBeforeSaving(Customer customer) {
		customer.setCpf(customer.getCpf().replaceAll("\\D+","")); //only digits
		return customer;
	}

}
