package io.platformbuilders.service;

import org.springframework.data.domain.Page;

import io.platformbuilders.model.Customer;

public interface CustomerService {
	
	Customer getCustomer(Long id);
	
	void saveCustomer(Customer customer);
	
	Customer removeCustomer(Long id);
	
	Customer updateCustomer(Long id, Customer customer);
	
	Customer updateCustomerName(Long id, String nome);
	
	Page<Customer> getCustomers(String nome, String cpf, int page, int size);
}
