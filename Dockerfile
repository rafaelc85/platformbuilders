FROM openjdk:8
ADD target/platformbuilders-rafael.jar platformbuilders-rafael.jar
EXPOSE 8089
ENTRYPOINT ["java", "-jar", "platformbuilders-rafael.jar"]