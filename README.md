
## Requirements

You must have the requirements below to run this project:

1. Have **Git** installed.
2. Have **Docker** installed.
3. Have **OpenJDK8** installed.
4. Have **Maven** installed.

---

## How to run

Next, you’ll need the following steps to run the project:

1. First step is to clone repository. You can do by running **git clone https://rafaelc85@bitbucket.org/rafaelc85/platformbuilders.git**
2. cd into **platformbuilders**
3. Generate project jar with Maven **mvn clean install**
4. Build the Docker Image: **docker build -t platformbuilders-rafael .**
5. Pull a MySQL image from Docker Hub: **docker pull mysql:5.6**
6. Create a MySQL container: **docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=customerdb -e MYSQL_USER=root -e MYSQL_PASSWORD=root -d mysql:5.6**
6. Start MySQL container: **docker start mysql-standalone**
7. Start application container **docker run -d -p 8089:8089 --name platformbuilders-rafael --link mysql-standalone:mysql platformbuilders-rafael**
8. The application is then available on **http://localhost:8089/customer**
9. There's a postman collection named **PlatformBuilders - Rafael.postman_collection.json** in the project to consume the customer API's

---
